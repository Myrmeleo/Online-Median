#include <cstdlib>
#include <vector>
#include <iostream>
#include <math.h>
#include "OnlineMedian.h"

using namespace std;

int OnlineMedian::root() {
    return 0;
}

int OnlineMedian::leftChild(int i, int n) {
    int p = 2 * i + 1;
    if (p > last(n)) {
        return -1;
    }
    return p;
}

int OnlineMedian::rightChild(int i, int n) {
    int p = 2 * i + 2;
    if (p > last(n)) {
        return -1;
    }
    return p;
}

int OnlineMedian::parent(int i) {
    int p = (i - 1) / 2;
    if (p < 0) {
        return -1;
    }
    return p;
}

int OnlineMedian::last(int n) {
    return n - 1;
}

void OnlineMedian::swap(vector<int> &v, int i, int j) {
    v[j] += v[i];
    v[i] = -v[i] + v[j];
    v[j] -= v[i];
}

void OnlineMedian::fixUpMax(vector<int> &v, int i) {
    //track the current node
    int n = i;
    int p = parent(n);
    while (p != -1 && v[p] < v[n]) {
        swap(v, n, p);
        n = p;
        p = parent(n);
    }
}

void OnlineMedian::fixUpMin(vector<int> &v, int i) {
    //track the current node
    int n = i;
    int p = parent(n);
    while (p != -1 && v[p] > v[n]) {
        swap(v, n, p);
        n = p;
        p = parent(n);
    }
}

void OnlineMedian::fixDownMax(vector<int> &v, int n, int i) {
    int c = i;
    int l = leftChild(c, n);
    while (l != -1) {
        if ((l != last(n) && v[rightChild(c, n)] > v[l])) {
            l = rightChild(c, n);
        }
        if (v[c] > v[l]) {
            break;
        }
        swap(v, c, l);
        c = l;
        l = leftChild(c, n);
    }
}

void OnlineMedian::fixDownMin(vector<int> &v, int n, int i) {
    int c = i;
    int l = leftChild(c, n);
    while (l != -1) {
        if (l != last(n) && v[rightChild(c, n)] < v[l]) {
            l = rightChild(c, n);
        }
        if (v[c] < v[l]) {
            break;
        }
        swap(v, c, l);
        c = l;
        l = leftChild(c, n);
    }
}

void OnlineMedian::insert(int x){
    int r = root();
    if (lowHalf.size() < highHalf.size()) {
        lowHalf.push_back(x);
        fixUpMax(lowHalf, last(lowHalf.size()));
        if (lowHalf[r] > highHalf[r]) {
            //swap the keys in the roots of the two heaps
            lowHalf[r] += highHalf[r];
            highHalf[r] = -highHalf[r] + lowHalf[r];
            lowHalf[r] -= highHalf[r];
            fixDownMin(highHalf, highHalf.size(), r);
        }
    } else {
        highHalf.push_back(x);
        fixUpMin(highHalf, last(highHalf.size()));
        if (lowHalf.size() > 0 && lowHalf[r] > highHalf[r]) {
            //swap the keys in the roots of the two heaps
            lowHalf[r] += highHalf[r];
            highHalf[r] = -highHalf[r] + lowHalf[r];
            lowHalf[r] -= highHalf[r];
            fixDownMax(lowHalf, lowHalf.size(), r);
        }
    }
}

int OnlineMedian::getMedian() {
    return highHalf[root()];
}


//read in a series of integers, printing the collection and the median at each integer
int main(){
    string line;
    OnlineMedian med = OnlineMedian();
    cout << "Enter any sequence of integers, one at a time." << endl;
    while(true) {
        if(!getline(cin, line)) {
            break;
        }
        int a = atoi(line.c_str());
        med.insert(a);
        cout << "reading: " << a << endl;
        cout << "median so far: " << med.getMedian() << endl;
    }
    return 0;
}
//g++ -std=c++17 *.cpp -o OnlineMedian