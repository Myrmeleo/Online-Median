#ifndef ONLINE_MEDIAN
#define ONLINE_MEDIAN


/*
Online Median stores a collection of numbers and can easily find the median at any time
The numbers are partitioned into 2 binary heaps
    lowHalf contains the smallest elements and is max-oriented
    highHalf contains the largest elements (including the median) and is min-oriented
The median is contained in the root node of lowHalf
*/
class OnlineMedian {
    std::vector<int> lowHalf;
    std::vector<int> highHalf;

    //root(v) returns the index of the root of any heap
    //Runtime: Theta(1)
    int root();
    
    //leftChild(i, n) returns the index of the left child of node i in n-size heap (-1 if it does not exist)
    //Runtime: Theta(1)
    int leftChild(int i, int n);
    
    //rightChild(i, n) returns the index of the right child of node i in n-size heap (-1 if it does not exist)
    //Runtime: Theta(1)
    int rightChild(int i, int n);

    //parent(i) returns the index of the parent of node i (-1 if it does not exist)
    //Runtime: Theta(1)
    int parent(int i);

    //last(n) returns the index of the last node of an n-size heap
    //Runtime: Theta(1)
    int last(int n);

    //swap(v, i, j) swaps the values in v at the indexes i and j 
    //Runtime: Theta(1)
    void swap(std::vector<int> &v, int i, int j);

    //fixUpMax(v, i) puts v into max-oriented heap structure working up from node i
    //Runtime: O(logn)
    void fixUpMax(std::vector<int> &v, int i);
    
    //fixUpMin(v, i) puts v into min-oriented heap structure working up from node i
    //Runtime: O(logn)
    void fixUpMin(std::vector<int> &v, int i);

    //fixDownMax(v, n, i) puts v of size n into max-oriented heap structure working down from node i
    //Runtime: O(logn)
    void fixDownMax(std::vector<int> &v, int n, int i);

    //fixDownMin(v, n, i) puts v of size n into min-oriented heap structure working down from node i
    //Runtime: O(logn)
    void fixDownMin(std::vector<int> &v, int n, int i);

public:
    //insert(x) puts x into one of the 2 heaps
    //Runtime: O(logn)
    void insert(int x);

    //getMedian() returns the median of the total values
    //Runtime: Theta(1)
    int getMedian();
};

#endif