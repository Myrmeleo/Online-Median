Leo Huang

OnlineMedian uses two binary heaps (one min-oriented and the other max-oriented) to track the median of a sequence of integers. Accessing the median takes O(1) time, while adding a number to the sequence takes O(logn) time.

Start the program by double clicking the .exe or entering './OnlineMedian' on the command line. Enter your integers one at a time.